import * as description from './area.md';
import examples from './examples';

export default {
  description,
  examples,
};
