import * as description from './search_box.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-form-input',
};
